from httplib2 import Http
from urllib import urlencode
import urllib2
import time
import serial
import re
import StringIO
import csv
import pickle


onHW = True
codeExists = False

if onHW:
	ser = serial.Serial(
		port='/dev/ttyUSB0',
		baudrate=115200,
		bytesize=8,
		parity='N',
		stopbits=1,
		timeout=None
		)
	ser.close()
	ser.open()
	ser.isOpen()

#Function defs
def internet_on():
    try:
        response=urllib2.urlopen('http://www.google.com',timeout=20)
        return True
    except urllib2.URLError as err: pass
    return False
#function defs end

########################################3
#Program development flow

# read csv locally
#parse data and sent to ser

#Download csv
#parse data and send to ser
######################################3
#Main loop Start


with open("blacklist", 'rb') as f:
    blacklist = pickle.load(f)

print blacklist

while (1):
	print "Loop Start--------------------------------------------------------"
	
	#loop till # is entered on keypad
	if onHW:
		tdata = ser.read(4)           # Wait forever for anything
		
	else:
		tdata = raw_input()
	
	time.sleep(1)              # Sleep (or inWaiting() doesn't give the correct value)
	
	if onHW:
		data_left = ser.inWaiting()  # Get the number of characters ready to be read
		tdata += ser.read(data_left) # Do the read and combine it with the first character
	
	read_line =tdata
	
	if onHW:
		read_line = ser.readline() #reads raw line
	
	linestr = read_line.rstrip() # removes extra newline

	print linestr
	
	##Handle # press with out pin entered
	
	if internet_on(): # send data if net is on
		print"Online"
		
		#retrive most updated file here
		url = 'http://black-electronics.com/vendingmachine/codes.csv'
		response = urllib2.urlopen(url)
		cr = csv.reader(response)
		codeExists = False
		for row in cr:
			print row
			if row[0]==linestr:
				print "hit"
				print row[1]
				codeExists = True
							
				#Black list codes here
								
				if row[0] not in blacklist:
					print "Valid"
					#ser.write(row[1]+"\n")
					ser.write(row[1]+"\n")
					blacklist.append(row[0])
					#write list to file
					with open("blacklist", 'wb') as f:
						pickle.dump(blacklist, f)
						
						
				else:
					print "Code used"
					ser.write("40") #40 for code used
				
				print blacklist
				break
			else:
				
				print "miss"
		
		if codeExists==False:#if not true 
			print "Code Not Found"		
			ser.write("41") #41 for code not found
					
		
	else:
		print "offline"
		ser.write("42") #41 for code not found
		#send message to HW as well
		
		#Dont do this
		# parse from csv locally 
		#print content


