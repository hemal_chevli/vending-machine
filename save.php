<?php
	
	date_default_timezone_set("Asia/Kolkata");
	define("PASSWORD", "p");
	define("LOG_FILE", "./codes.csv");
	
	
	if(!isset($_GET["code"])) die("KO - no value present");
	if(!isset($_GET["item"])) die("KO - no value present");
	if(!isset($_GET["pwd"])) die("KO - no password present");
	
	$code = $_GET["code"];
	if(!is_numeric($code)) die("KO - code is not a number");
	
	$item = $_GET["item"];
	if(!is_numeric($item)) die("KO - item is not a number");
	
	$pwd = $_GET["pwd"];
	if($pwd != PASSWORD) die("KO - invalid password");		
		
	$file_handler = fopen(LOG_FILE, "a+");
	$timestamp=time();
	$time = gmdate("Y-m-d\ T H:i:s", $timestamp);	

	fwrite($file_handler, $code . "," . $item . "\n");
	
	fflush($file_handler);
	echo "OK,data saved in csv file";
?>
