//Tune motors to its positions

//send motor from serial. that motor will start to turn.
//send "s" to stop all motors

void setup(){
  Serial.begin(115200);

}
void loop(){
  if(Serial.available()>0){
    int motor = Serial.parseInt();
    //motor becomes 0 after it gets number
    //Serial.print(motor);    //only for serial debugging
    //print temp for serial terminal only  

    if(motor !=0){

      dispense(motor);
      //clear LCD

    }
  }//if ser availalbe end

}
void dispense(int m){
  int motor = m+21;//tune this
  pinMode(motor,OUTPUT);
  digitalWrite(motor,HIGH);
  delay(3000);//tune this
  digitalWrite(motor,LOW);
  pinMode(motor,INPUT);
}

