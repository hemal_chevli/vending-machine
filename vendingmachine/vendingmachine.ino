#include <LiquidCrystal595.h>

#include <Keypad.h>

const byte ROWS = 4; // Four rows
const byte COLS = 4; // Four columns
// Define the Keymap
char keys[ROWS][COLS] = {
  {
    '1','2','3','A'                }
  ,
  {
    '4','5','6','B'                }
  ,
  {
    '7','8','9','C'                }
  ,
  {
    '*','0','#','D'                }
};
// Connect keypad ROW0, ROW1, ROW2 and ROW3 to these Arduino pins.
byte rowPins[ROWS] = { 
  12,11,10,9 };



// Connect keypad COL0, COL1 and COL2 to these Arduino pins.
byte colPins[COLS] = { 
  8,7,6,5 }; 



// Create the Keypad
Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


//data latch clk
LiquidCrystal595 lcd(2,3,4);

char inchar;
char temp[16];
int i;
int z=0;
char attempt[4]={
  '0','0','0','0'};

int toLCD=0;
int toMotor=0;
int motor=0;

//prototype declaration
void dispense(int m);
void setup()
{
  lcd.begin(16,2); 
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Enter Code:");
  lcd.setCursor(0,1);
  Serial.begin(115200);
  Serial1.begin(115200);

  Serial1.println("Ready");
  Serial1.println(motor);
  //Serial.println(attempt);

}

void loop()
{
  char key = kpd.getKey();
  if(key)  // Check for a valid key.
  {



    switch (key)
    {
    case '*':
      z=0;
      lcd.setCursor(0,0);
      lcd.print("Enter Code:");
      lcd.setCursor(0,1);
      lcd.print("                ");
      lcd.setCursor(0,1);
      //re enter code
      break;
    case '#':
      //get message from pc
      // lcd.clear();
      // lcd.setCursor(0,0);
      //lcd.print("Please Wait");
      //send code array
      Serial.println(attempt);
      delay(100);
      //python is not reading in first go.
      Serial.println(attempt);
      z=0;

      lcd.setCursor(0,0);
      lcd.print("                ");
      lcd.setCursor(0,0);
      lcd.print("Enter Code:");
      lcd.setCursor(0,1);
      lcd.print("                ");
      lcd.setCursor(0,1);
      //clear array
      attempt = {'0','0','0','0'};

      break;
    default:

      if(z<4){
        lcd.print(key);//prints on 4 digits to lcd.
        attempt[z]=key;
        z++;
      }
      //print key on lcd

      //Send code all at once when # is pressed
    }//sw end
  }//if key end

  //lcd.print(temp);
  if (Serial.available()>0) // Checks for a character in the serial monitor
  { 
    int motor = Serial.parseInt();
    //motor becomes 0 after it gets number
    //Serial.print(motor);    //only for serial debugging
    //print temp for serial terminal only  
    Serial1.println(motor); //send data to serial monitor for debugging
    if(motor == 40){
      //code used
      lcd.clear();
      lcd.print("PIN USED");
      delay(4000);
      lcd.clear();
      lcd.print("Enter Code:");
      lcd.setCursor(0,1);
      lcd.print("                ");
      lcd.setCursor(0,1);
      Serial1.println("Pin used");

    }

    else if(motor == 41){
      //code not found
      lcd.clear();
      lcd.print("PIN NOT FOUND");
      delay(4000);
      lcd.clear();
      lcd.print("Enter Code:");
      lcd.setCursor(0,1);
      lcd.print("                ");
      lcd.setCursor(0,1);
      Serial1.println("Pin not found");

    }
    else if(motor == 42){
      //No internet
      lcd.clear();
      lcd.print("SYSTEM OFFLINE");
      delay(4000);
      lcd.clear();
      lcd.print("Enter Code:");
      lcd.setCursor(0,1);
      lcd.print("                ");
      lcd.setCursor(0,1);
      Serial1.println("offline");

    }

    else if(motor >0 && motor < 31){
      //Display Item number to be dispensed
      lcd.clear();
      lcd.print("Item Number:");
      lcd.print(motor);

      dispense(motor);

      lcd.clear();
      lcd.print("Enter Code:");
      lcd.setCursor(0,1);
      lcd.print("                ");
      lcd.setCursor(0,1);
      Serial1.println("dispensed");
    }
  }//if ser availalbe end

}//loop end

void dispense(int m){
  int motor = m+21;//motor 1 is at D22
  pinMode(motor,OUTPUT);
  digitalWrite(motor,HIGH);
  delay(3000);//tune this
  digitalWrite(motor,LOW);
  pinMode(motor,INPUT);
}


